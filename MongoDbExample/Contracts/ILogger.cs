﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MongoDbExample.Contracts
{
    interface ILogger
    {
        void logRequestBodyData(string requestBodyLogData);
        void logResponseBodyData(string responseBodyLogData);
    }
}
