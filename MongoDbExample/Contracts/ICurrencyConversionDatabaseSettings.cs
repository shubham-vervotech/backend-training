﻿namespace MongoDbExample.Contracts
{
    public interface ICurrencyConversionDatabaseSettings
    {
        string CurrencyConversionCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}