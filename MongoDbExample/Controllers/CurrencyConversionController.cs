﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MongoDbExample.Logger;
using MongoDbExample.Models;
using MongoDbExample.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace MongoDbExample.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CurrencyConversionController : ControllerBase
    {
        //readonly ILogger<CurrencyConversionController> _log;
        private readonly FileLogger _log;
        private readonly CurrencyConversionService _currencyConversionService;
        public CurrencyConversionController(CurrencyConversionService service, FileLogger log)
        {
            _currencyConversionService = service;
            _log = log;
        }
        [HttpPost]
        public async Task<CurrencyConversionResponse> ConvertCurrency(CurrencyConversionRequest conversionRequestData)
        {
            try
            {

                string requestBodyLogData = JsonConvert.SerializeObject(conversionRequestData);
                Debug.WriteLine("controller");
                _log.logRequestBodyData(requestBodyLogData);
                if (!ModelState.IsValid)
                {
                    throw new Exception("Invalid Request Body");
                }
                var response = await _currencyConversionService.ConvertCurrency(conversionRequestData);
                string responseBodyLogData = JsonConvert.SerializeObject(response);
                _log.logResponseBodyData(responseBodyLogData);
                return response;
            }
            catch (Exception error)
            {
                return new CurrencyConversionResponse()
                {
                    status = false,
                    currencyConversionList = null,
                    message = error.Message
                };
            }
        }
    }
}
