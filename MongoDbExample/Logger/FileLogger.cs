﻿using MongoDbExample.Contracts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MongoDbExample.Logger
{
    public class FileLogger : ILogger
    {
        public void logRequestBodyData(string requestBodyLogData)
        {
            var currentDatetime = DateTime.Now.Ticks;
            File.WriteAllText($"request.{currentDatetime}", requestBodyLogData);
        }

        public void logResponseBodyData(string responseBodyLogData)
        {
            var currentDatetime = DateTime.Now.Ticks;
            File.WriteAllText($"response.{currentDatetime}", responseBodyLogData);
        }
    }
}