﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MongoDbExample.Models
{
    public class CurrencyConversionResponse
    {
        public List<ConvertedCurrencyList> currencyConversionList { get; set; }
        public Boolean status { get; set; }
        public string message { get; set; }
    }
    public class ConvertedCurrencyList
    {
        public double amount { get; set; }
        public double? updatedAmount { get; set; }

        public string sourceCurrencyType { get; set; }
        public string targetCurrencyType { get; set; }

    }
}
