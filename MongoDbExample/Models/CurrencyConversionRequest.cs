﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MongoDbExample.Models
{
    public class CurrencyConversionRequest
    {
        public List<conversionRequestData> currencyConversionList { get; set; }
    }
    public class conversionRequestData
    {
        public float amount { get; set; }
        public string sourceCurrencyType { get; set; }
        public string targetCurrencyType { get; set; }

    }
}