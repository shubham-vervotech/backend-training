﻿using MongoDbExample.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MongoDbExample.Models
{
    public class CurrencyConversionDatabaseSettings : ICurrencyConversionDatabaseSettings
    {
        public string CurrencyConversionCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}
