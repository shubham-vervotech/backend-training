﻿using MongoDbExample.Models;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDbExample.Contracts;
using System.Diagnostics;
using System;

namespace MongoDbExample.Services
{
    public class CurrencyConversionService
    {
        private readonly IMongoCollection<currenyConversion> _currenyConversion;
        public CurrencyConversionService(ICurrencyConversionDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            _currenyConversion = database.GetCollection<currenyConversion>(settings.CurrencyConversionCollectionName);
        }
        public async Task<CurrencyConversionResponse> ConvertCurrency(CurrencyConversionRequest conversionRequestData)
        {
            CurrencyConversionResponse responseObject = new CurrencyConversionResponse();
            try
            {
                if(conversionRequestData.currencyConversionList.Count == 0)
                {
                    throw new Exception("No data provided for currency conversion");
                }
                
                List<ConvertedCurrencyList> convertedCurrencyList = new List<ConvertedCurrencyList>();
                foreach (var coversionItem in conversionRequestData.currencyConversionList)
                {
                    if (!(coversionItem.sourceCurrencyType is string && coversionItem.targetCurrencyType is string))
                    {
                        throw new Exception("Source and Target currency value should be a string");
                    }
                    ConvertedCurrencyList convertedCurrencyObj = new ConvertedCurrencyList();
                    Debug.WriteLine(coversionItem.sourceCurrencyType + "  type");
                    var ConversionRecord = await _currenyConversion.Find<currenyConversion>(s => s.baseCurrency == coversionItem.sourceCurrencyType).FirstOrDefaultAsync();
                    convertedCurrencyObj.sourceCurrencyType = coversionItem.sourceCurrencyType;
                    convertedCurrencyObj.targetCurrencyType = coversionItem.targetCurrencyType;
                    convertedCurrencyObj.amount = coversionItem.amount;
                    if (ConversionRecord != null)
                    {
                        Debug.WriteLine(ConversionRecord.rates.GetType().GetProperty(coversionItem.targetCurrencyType).GetValue(ConversionRecord.rates, null) + "  222"); ;
                        double convertedAmount = (double)ConversionRecord.rates.GetType().GetProperty(coversionItem.targetCurrencyType).GetValue(ConversionRecord.rates, null) * coversionItem.amount;
                        Debug.WriteLine(convertedAmount + "  converted rate");
                        convertedCurrencyObj.updatedAmount = convertedAmount;
                    }
                    else
                    {
                        convertedCurrencyObj.updatedAmount = null;
                    }
                    convertedCurrencyList.Add(convertedCurrencyObj);

                }
                responseObject.currencyConversionList = convertedCurrencyList;
                responseObject.status = true;
                responseObject.message = "Currencies Converted Successfully";
                // Debug.WriteLine(ConversionRecord + "  service");
            }
            catch (Exception error)
            {
                responseObject.status = false;
                responseObject.message = error.Message;
            }
            return responseObject;
        }
    }
}
